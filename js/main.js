const COMPONENT_NAME = 'wiki-uls';
const defaultTemplate = `<div class="${COMPONENT_NAME}-container">
<slot name="header"></slot>
<form class="search-form">
<input type="search" class="search-input" spellcheck="false" autofocus/>
</form>

<section class="no-results" >
<slot name="no-results">
Could not find any results.
</slot>
</section>

<section class="search-results"></section>

</div>`

const baseStyle = `
<style>
.${COMPONENT_NAME}-container {
    width: clamp(20ch, 40vw, 100ch);
    font-family: sans-serif;
    border: 1px solid #ccc;
    padding: 10px;
    max-height: 60vh;
    height: 25lh;
    overflow: hidden;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);

    &.few-languages {
        width: clamp(20ch, 20vw, 30ch);
    }
    &.some-languages {
        width: clamp(20ch, 30vw, 50ch);
    }
}


.search-form {
    display: flex;
}

.search-input {
    flex: 1;
    padding: 5px;

    border-radius: 5px;
    font-size: 1em;
    border: none;

    width: 60ch;

    padding: 1em;
    width: 20px;

    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMiIgaGVpZ2h0PSIxMyI+PGcgc3Ryb2tlLXdpZHRoPSIyIiBzdHJva2U9IiM2YzZjNmMiIGZpbGw9Im5vbmUiPjxwYXRoIGQ9Ik0xMS4yOSAxMS43MWwtNC00Ii8+PGNpcmNsZSBjeD0iNSIgY3k9IjUiIHI9IjQiLz48L2c+PC9zdmc+) no-repeat;
    background-position: 5px center;
    background-size: 20px;
    text-indent: 25px;
}

.search-results {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    align-content: flex-start;
    gap: 5px;
    overflow-y: auto;
    scrollbar-width: thin;
    height: 22lh;
    padding: 0;
    list-style-type: none;
    margin-top: 1em;

    .search-result {
        padding: 5px;
        line-height: 1.5;
        flex-grow: 1;
        width: 20ch;
        cursor: pointer;
        background-color: transparent;
        border: none;
        text-align: start;
        font-size: 1em;
        height: 1.5lh;
        &:active,
        &:hover {
            background-color: revert;
            transition: background-color 0.2s;
        }
        &:nth-child(12n)  {
            margin-bottom: 1lh;
        }
    }
}
.no-results {
    margin-top: 2lh;
    display: none;
    color: #f00;
}
</style>
`

/**
 * Represents a custom HTML element for the WikiULS component.
 * @extends HTMLElement
 */
class WikiULS extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.baseStyle = baseStyle;
        if (document.getElementById(`${COMPONENT_NAME}-template`)) {
            this.template = document.querySelector('template').innerHTML;
        } else {
            this.template = defaultTemplate;
        }

        this.placeholder = this.getAttribute("placeholder") || "Search languages";
        this.attachShadow({ mode: 'open', delegatesFocus: true }).innerHTML = this.baseStyle + this.template;
        this.container = this.shadowRoot.querySelector(`.${COMPONENT_NAME}-container`);
        this.searchForm = this.shadowRoot.querySelector('.search-form');

        this.searchInput = this.shadowRoot.querySelector('.search-input');
        this.resultsContainer = this.shadowRoot.querySelector('.search-results');
        this.searchInput.setAttribute('placeholder', this.placeholder);

        const allLanguages = Object.keys(wikimedialanguageData.getLanguages());
        this.languages = allLanguages;
        this.searchInput.addEventListener('input', () => this.onSearch());

        this.searchForm.addEventListener('submit', (event) => {
            event.preventDefault();
            this.onSearch();
        });

        // Focus the search input when the popover is opened
        this.addEventListener("toggle", (event) => {
            if (event.newState === "open") {
                this.onOpen()
            }
        });
    }

    onOpen() {
        const customLanguages = this.getAttribute("data-languages")
        if (customLanguages) {
            this.languages = JSON.parse(customLanguages);
        }

        if (typeof this.languages === 'object' && !Array.isArray(this.languages)) {
            this.languageData = this.languages;
            this.languages = Object.keys(this.languages);
        }

        const numberOfLanguages = this.languages.length;
        let selectorClass = "many-languages"
        if (numberOfLanguages <= 10) {
            selectorClass = "few-languages"
        } else if (numberOfLanguages <= 30) {
            selectorClass = "some-languages"
        };
        this.container.classList.add(selectorClass);

        this.focus();
        this.onSearch();
        this.dispatchEvent(new CustomEvent('toggle'));
    }

    async onSearch() {
        const query = this.searchInput.value;
        try {
            const results = await this.search(this.languages, query);
            if (results.length === 0) {
                this.shadowRoot.querySelector('.no-results').style.display = 'block';
            } else {
                this.shadowRoot.querySelector('.no-results').style.display = 'none';
            }
            this.renderResults(results);
        } catch (error) {
            console.error('Error fetching search results:', error);
            this.resultsContainer.style.display = 'none';
            this.shadowRoot.querySelector('.no-results').style.display = 'block';
        }
    }

    clear() {
        this.searchInput.value = '';
        this.resultsContainer.innerHTML = '';
        this.shadowRoot.querySelector('.no-results').style.display = 'none';
    }



    /**
     * Searches for languages based on the given query.
     * @param {string[]} languages - An array of language codes to search within.
     * @param {string} query - The search query.
     * @returns {Promise<string[]>} - A promise that resolves to an array of language codes that match the search query.
     */
    async search(languages, query) {
        const LANGUAGE_SERARCH_API =
            "https://en.wikipedia.org/w/api.php?action=languagesearch&format=json&formatversion=2";


        if (!query || query.trim().length === 0) {
            return languages;
        }

        // See if the search query is a language code
        const exactMatch = languages.filter(
            (code) => query.toLowerCase() === code.toLowerCase()
        );
        if (exactMatch.length) {
            return exactMatch;
        }

        const filterResults = languages.filter(
            (code) =>
                // Search using autonym
                wikimedialanguageData
                    .getAutonym(code)
                    .toLowerCase()
                    .includes(query.toLowerCase()) ||
                // Search using script name
                wikimedialanguageData.getScript(code).toLowerCase().includes(query.toLowerCase())
        );

        if (filterResults.length) {
            return filterResults;
        }

        // We did not find any results from client side search.
        // Attempt a search using the given search API
        if (LANGUAGE_SERARCH_API) {
            const searchApiResults = await this.searchWithAPI(LANGUAGE_SERARCH_API, query);

            // Remove the languages not known to this selector.
            return searchApiResults.filter((code) => languages.includes(code));
        }

        return [];
    };


    /**
     * Searches with the provided API and returns the list of languages matching the query.
     *
     * @param {string} searchAPI - The URL of the search API.
     * @param {string} query - The search query.
     * @returns {Promise<string[]>} - A promise that resolves to an array of language keys.
     */
    searchWithAPI(searchAPI, query) {
        const apiURL = new URL(searchAPI);

        apiURL.searchParams.append("search", query);
        apiURL.searchParams.append("origin", "*");

        return fetch(apiURL.toString())
            .then((response) => response.json())
            .then((result) => Object.keys(result.languagesearch || {}));
    };

    renderResults(languages) {
        this.resultsContainer.style.display = 'flex';
        this.resultsContainer.innerHTML = '';
        let resultObjs = languages.map(lang => {
            return {
                lang,
                autonym: wikimedialanguageData.getAutonym(lang),
                dir: wikimedialanguageData.getDir(lang),
                data: this.languageData ? this.languageData[lang] : null
            };
        })

        resultObjs = resultObjs.sort((a, b) => a.autonym.localeCompare(b.autonym));

        resultObjs.forEach(result => {
            this.resultsContainer.appendChild(this.renderResult(result));
        });
    }

    /**
     * Renders a search result element based on the provided result object.
     *
     * @param {Object} result - The result object containing information about the search result.
     * @param {string} result.lang - The language code of the language.
     * @param {string} result.autonym - The autonym of the language.
     * @param {string} result.dir - The direction of the language.
     * @param {object} result.data - The data mapped for the language.
     * @returns {HTMLButtonElement} - The rendered search result element.
     */
    renderResult(result) {
        const resultElement = document.createElement('button');
        resultElement.classList.add('search-result');
        resultElement.setAttribute('lang', result.lang);
        resultElement.setAttribute('role', 'button');
        resultElement.setAttribute('tabindex', '0');
        resultElement.setAttribute('title', `${result.autonym} - ${result.lang}`);
        resultElement.textContent = result.autonym;
        resultElement.addEventListener('click', () => {
            this.dispatchEvent(new CustomEvent('select', {
                detail: result
            }));

            this.togglePopover();
        });

        return resultElement;
    };

}

if (!customElements.get(COMPONENT_NAME)) {
    // if already defined, do nothing (e.g. same script imported twice)
    customElements.define(COMPONENT_NAME, WikiULS);
}


class WikiArticleULS extends WikiULS {
    renderResult(result) {
        const resultElement = document.createElement('a');
        resultElement.classList.add('search-result');
        resultElement.setAttribute('lang', result.lang);
        resultElement.setAttribute('role', 'button');
        resultElement.setAttribute('tabindex', '0');
        resultElement.setAttribute('href', `https://${result.lang}.wikipedia.org/wiki/${result.data}`);
        resultElement.setAttribute('title', `${result.data}`);
        resultElement.textContent = result.autonym;
        return resultElement;
    };
}

customElements.define("wiki-article-uls", WikiArticleULS);

